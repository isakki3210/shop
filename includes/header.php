<form action="./search.php" method="post">
    <header class="bg-dark py-5">
        <div class="container px-4 px-lg-5 my-5">
            <div class="text-white">
                <h1 class="display-4 fw-bolder">Shop in style</h1>
                <div class="input-group mb-3 mt-4 w-75">
                    <input type="text" class="form-control" placeholder="Search the Product" autocomplete="off" name="search" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                </div>
            </div>
        </div>
    </header>
</form>