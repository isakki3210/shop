<?php
if (isset($_GET['cart_id'])) {
  $cart_id = $_GET['cart_id'];

  $delete_cart_query = "DELETE FROM cart WHERE cart_id = $cart_id";
  $delete_cart_res = mysqli_query($connection, $delete_cart_query);
  if ($delete_cart_res) {
    header('location:shop_cart_.php');
  }
}
?>
<section class="h-100 gradient-custom">
  <div class="container py-5">
    <div class="row d-flex justify-content-center my-4">
      <div class="col-md-8">
        <div class="card mb-4">
          <div class="card-header py-3 position-relative ">
            <h5 class="mb-0">Shopping Cart</h5>
            <a href="index.php" class="link-dark">
              <h5 class="mb-0 float-end" style="margin-top: -22px; font-size:15px;"><i class="bi bi-arrow-up-left-circle-fill"></i> Continue Shopping</h5>
            </a>
          </div>
          <div class="card-body">
            <?php
            $total_price = NULL;
            $user_id = $_SESSION['id'];
            $select_cart_query = "SELECT * FROM cart WHERE user_id = '$user_id'";
            $select_cart_query_res = mysqli_query($connection, $select_cart_query);
            $dress_id = NULL;
            $search_count = mysqli_num_rows($select_cart_query_res);

            if ($search_count === 0) {
            ?>
              <div>
                <h4 class="justify-content-center">Your Shopping Cart is Empty</h4>
                <p>
                  Return to the store to add items for your delivery slot.
                  Before proceed to checkout you must add some products to your shopping cart.
                  You will find a lot of interesting Products on Our Shop Page
                </p>
                <a class="btn btn-info" href="index.php"><i class="bi bi-bag-check"></i> ShopNew</a>
              </div>
            <?php
            }

            while ($row = mysqli_fetch_assoc($select_cart_query_res)) {
              $dress_id = $row['pop_id'];
              $quantity = $row['quantity'];
              $size = $row['size'];
              $cart_id = $row['cart_id'];

              $select_cart_dress_query = "SELECT * FROM popular_item WHERE id = $dress_id";
              $select_cart_dress_res = mysqli_query($connection, $select_cart_dress_query);
              // echo $dress_id;
              while ($row = mysqli_fetch_assoc($select_cart_dress_res)) {
                $pop_img = $row['pop_img'];
                $pop_name = $row['pop_name'];
                $price = $row['pop_price'];
                $rating = $row['pop_star'];
                $id = $row['id'];

                if ($quantity > 1) {
                  $price *= $quantity;
                }

                if (isset($_GET['value'])) {
                  $value = $_GET['value'];

                  if ($value == 'add') {
                    $quantity += 1;
                    $value_update_query = "UPDATE cart SET quantity = $quantity WHERE cart_id = $cart_id";
                    $value_update_res = mysqli_query($connection, $value_update_query);
                    header('location:shop_cart_.php');
                  }

                  if ($value == 'minus') {
                    $quantity -= 1;
                    $value_update_query = "UPDATE cart SET quantity = $quantity WHERE cart_id = $cart_id";
                    $value_update_res = mysqli_query($connection, $value_update_query);
                    header('location:shop_cart_.php');
                    // header("location:javascript://history.go(-1)");
                  }
                }
                if ($quantity == 0) {
                  $delete_cart_query = "DELETE FROM cart WHERE cart_id = $cart_id";
                  $delete_cart_res = mysqli_query($connection, $delete_cart_query);
                  if ($delete_cart_res) {
                    header('location:shop_cart_.php');
                    // header("location:javascript://history.go(-1)");
                  }
                }
              }
            ?>
              <hr class="my-4" />
              <div class="row">
                <div class="col-lg-3 col-md-12 mb-4 mb-lg-0">
                  <div class="bg-image hover-overlay hover-zoom ripple rounded" data-mdb-ripple-color="light">
                    <img src="img/<?php echo $pop_img; ?>" class="w-100 h-100" />
                  </div>
                </div>
                <div class="col-lg-5 col-md-6 mb-4 mb-lg-0">
                  <p><strong>
                      <a href="./cart.php?pop_id=<?php echo $id; ?>" class="text-decoration-none link-dark">
                        <?php echo $pop_name ?></strong></p>
                  </a>
                  <p>Size: <?php
                            if ($size == 'Size') {
                              echo '0';
                            } else {
                              echo $size;
                            }

                            ?></p>
                  <p>
                  <div class="d-flex justify-content small text-warning mb-2">
                    <?php
                    for ($i = 0; $i < $rating; $i++) {
                    ?>
                      <div class="bi-star-fill"></div>
                      <?php }
                    if ($rating < 5) {
                      for ($bal = $rating; $bal < 5; $bal++) {
                      ?>
                        <div class="bi-star"></div>
                    <?php }
                    }
                    ?>
                  </div>
                  </p>
                </div>
                <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
                  <div class="d-flex mb-4" style="max-width: 300px">
                    <a href="shop_cart_.php?value=minus" class="btn px-3 me-2" onclick="this.parentNode.querySelector('input[type=number]').stepDown()">
                      <i class="bi bi-dash"></i>
                    </a>
                    <div class="form-outline">
                      <input id="form1" min="0" name="quantity" value="<?php echo $quantity ?>" type="number" class="form-control" />
                    </div>
                    <a href="shop_cart_.php?value=add" class="btn px-3 ms-2" onclick="this.parentNode.querySelector('input[type=number]').stepUp()">
                      <i class="bi bi-plus"></i>
                    </a>
                    <a type="button" href="shop_cart_.php?cart_id=<?php echo $cart_id ?>" class=" btn btn-outline-info" data-mdb-toggle="tooltip" title="Remove item">
                      <i class="bi bi-trash"></i>
                    </a>
                  </div>
                  <p class="text-start text-md-center">
                    <strong>$<?php echo $price ?></strong>
                  </p>
                </div>
              </div>
            <?php
              $total_price += $price;
            }
            ?>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="card mb-4">
          <div class="card-header py-3">
            <h5 class="mb-0">Summary</h5>
          </div>
          <div class="card-body">
            <ul class="list-group list-group-flush">
              <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 pb-0">
                Products
                <span>$<?php echo $total_price ?></span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center px-0">
                GST
                <span>$0</span>
              </li>
              <li class="list-group-item d-flex justify-content-between align-items-center border-0 px-0 mb-3">
                <div>
                  <strong>Total amount</strong>
                  <strong>
                    <p class="mb-0">(including TAX)</p>
                  </strong>
                </div>
                <span><strong>$<?php echo $total_price ?></strong></span>
              </li>
            </ul>
            <button type="button" class="btn btn-primary btn-lg btn-block">
              Go to checkout
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>