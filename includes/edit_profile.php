<?php ob_start();
$connection = mysqli_connect('localhost', 'root', '', 'shop');
$user_name = $_SESSION['username'];
$user_id = $_SESSION['id'];
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-white-900 mb-4">Edit My Profile</h1>
                                </div>
                                <?php
                                $ViewProfile = "SELECT * FROM users WHERE user_id = $user_id";
                                $ViewProfile_query = mysqli_query($connection, $ViewProfile);
                                while ($row = mysqli_fetch_assoc($ViewProfile_query)) {
                                    $f_name = $row['user_firstname'];
                                    $l_name = $row['user_lastname'];
                                    $name = $row['username'];
                                    $email = $row['user_email'];
                                }

                                if (isset($_POST['update_user'])) {
                                    $u_name = $_POST['username'];
                                    $u_email = $_POST['email'];
                                    $uf_name = $_POST['f_name'];
                                    $ul_name = $_POST['l_name'];


                                    $update_query = "UPDATE users SET username = '$u_name' , user_firstname = '$uf_name' , user_lastname = '$ul_name' , user_email = '$u_email' WHERE user_id = $user_id";
                                    $u_res = mysqli_query($connection, $update_query);
                                    header('location: profile.php');
                                    $_SESSION['username'] = $u_name;
                                }
                                ?>
                                <form method="post">
                                    <img src="img/<?php echo $_SESSION['img']; ?>" class="img-profiles rounded-circle mb-4" alt="Avatar">
                                    <input type="file">
                                    <div class="input-group mb-3">
                                        <!-- <?php echo $u_name; ?> -->
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                        <input type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1" value="<?php echo $name ?>" name="username">
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2" value="<?php echo $email ?>" name="email">
                                        <span class="input-group-text" id="basic-addon2">@example.com</span>
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">First Name</span>
                                        <input type="text" aria-label="First name" class="form-control" value="<?php echo $f_name ?>" name="f_name">
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Last Name</span>
                                        <input type="text" aria-label="Last name" class="form-control" value="<?php echo $l_name ?>" name="l_name">
                                    </div>
                                    <button class="btn btn-outline-primary text-center float-end mb-3" name="update_user" type="submit">
                                        Proceed
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>