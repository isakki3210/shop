<?php

if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] !== 'user') {
        session_destroy();
        header('location: ./login.php');
    }
} else {
    session_destroy();
    header('location: ./login.php');
}
include './includes/db.php';
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand nav-font" href="index.php"><i class="bi bi-bag-check"></i> ShopNew</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link active mt-1" aria-current="page" href="index.php">Home</a></li>
            </ul>
            <?php
            $user_id = $_SESSION['id'];
            $select_cart = "SELECT * FROM cart WHERE user_id = '$user_id'";
            $cart_query = mysqli_query($connection, $select_cart);
            $sum = 0;
            while ($row  = mysqli_fetch_assoc($cart_query)) {
                $cart_quantity = $row['quantity'];
                $sum += $cart_quantity;
            }

            ?>

            <div class="d-flex float-end">
                <a href="shop_cart_.php" class="btn btn-outline-dark">
                    <i class="bi-cart-fill me-1"></i>
                    Cart
                    <span class="badge bg-dark text-white ms-1 rounded-pill"><?php echo $sum ?></span>
                </a>
            </div>
            <style>
                .rounded-circle {
                    border-radius: 50% !important;
                }

                .img-profile {
                    height: 2rem;
                    width: 2rem;
                }

                .img-profiles {
                    height: 6rem;
                    width: 6rem;
                }

                .nav-font {
                    font-family: 'Dancing Script', cursive;
                    font-size: 22px;
                }
            </style>
            <ul class="navbar-nav d-flex">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-dark " id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="img/<?php echo $_SESSION['img']; ?>" class="img-profile rounded-circle ms-2" alt="Avatar">
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="profile.php"><i class="bi bi-person-fill me-2"></i><?php echo $_SESSION['username']; ?></a></li>
                        <li><a class="dropdown-item" href="logout.php"><i class="bi bi-box-arrow-in-right me-2"></i>Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>