<?php ob_start();
// include './includes/db.php';
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-white-900 mb-4">My Profile</h1>
                                </div>
                                <?php
                                $user_id = $_SESSION['id'];
                                $ViewProfile = "SELECT * FROM users WHERE user_id = $user_id";
                                $ViewProfile_query = mysqli_query($connection, $ViewProfile);

                                while ($row = mysqli_fetch_assoc($ViewProfile_query)) {
                                    $f_name = $row['user_firstname'];
                                    $l_name = $row['user_lastname'];
                                    $name = $row['username'];
                                    $email = $row['user_email'];
                                }
                                ?>
                                <a href="./profile.php?page=edit_profile&user=<?php echo $_SESSION['username']; ?>" type="submit" class="btn btn-outline-secondary text-center float-end mb-3" name="edit">Edit</a>
                                <form action="" method="post">
                                    <img src="img/<?php echo $_SESSION['img']; ?>" class="img-profiles rounded-circle mb-4" alt="Avatar">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                        <input type="text" disabled class="form-control" aria-label="Username" name="username" aria-describedby="basic-addon1" value="<?php echo $name ?>">
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" disabled name="email" aria-label="Recipient's username" aria-describedby="basic-addon2" value="<?php echo $email ?>">
                                        <span class="input-group-text" id="basic-addon2">@example.com</span>
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">First Name</span>
                                        <input type="text" aria-label="First name" class="form-control" name="f_name" disabled value="<?php echo $f_name ?>">
                                        <!-- <input type="text" aria-label="Last name" class="form-control" name="l_name" placeholder="Last Name"> -->
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Last Name</span>
                                        <!-- <input type="text" aria-label="First name" class="form-control" name="f_name" placeholder="First Name"> -->
                                        <input type="text" aria-label="Last name" class="form-control" name="l_name" disabled value="<?php echo $l_name ?>">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>