-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2023 at 07:30 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.0.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `pop_id` int(32) NOT NULL,
  `quantity` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `pop_id`, `quantity`) VALUES
(8, 2, 1, 2),
(9, 2, 3, 4),
(17, 3, 4, 2),
(18, 3, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `popular_item`
--

CREATE TABLE `popular_item` (
  `id` int(200) NOT NULL,
  `pop_name` varchar(200) NOT NULL,
  `pop_img` varchar(200) NOT NULL,
  `pop_price` int(32) NOT NULL,
  `pop_star` int(32) NOT NULL,
  `dress_type` varchar(200) NOT NULL,
  `common_dress` varchar(200) NOT NULL,
  `cart` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `popular_item`
--

INSERT INTO `popular_item` (`id`, `pop_name`, `pop_img`, `pop_price`, `pop_star`, `dress_type`, `common_dress`, `cart`) VALUES
(1, 'Men Cargos', 'cargos.jpg', 200, 4, 'cargos', 'cargoss.jpeg', 2),
(2, 'Men Blue Jeans', 'jean.jpg', 300, 5, 'jean', 'jeans.jpeg', 0),
(3, 'Men Black Jeans', 'jean1.jpg', 100, 5, 'jean', 'jeans.jpeg', 0),
(4, 'Men Trouser', 'trouser.jpg', 235, 4, 'trouser', 'trousers.jpeg', 1),
(5, 'Women Black Jean', 'womenjean.jpg', 345, 4, 'women jean', 'womenjeans.jpeg', 0),
(6, 'Women Gray Jean', 'womenjean1.jpg', 300, 5, 'women jean', 'womenjeans.jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(32) NOT NULL,
  `username` varchar(200) NOT NULL,
  `user_firstname` varchar(200) NOT NULL,
  `user_lastname` varchar(200) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `user_image` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_role` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `user_firstname`, `user_lastname`, `user_password`, `user_image`, `user_email`, `user_role`) VALUES
(1, 'Isakki', 'Isakki', 'K', '$2a$07$usesomesillystringisaeoV8IUh.vueh3gu7KQn3ZNZKVLuGYmEe', 'userIsakki.jpeg', 'access.isakki@gmail.com', 'Admin'),
(2, 'Esakki', 'Esakkimuthu', 'K', '$2a$07$usesomesillystringisaeoV8IUh.vueh3gu7KQn3ZNZKVLuGYmEe', 'avatar.png', 'esakki3210@gmail.com', 'user'),
(3, 'Yuva', 'Yuvarajapandian', 'M', '$2a$07$usesomesillystringisaef9Xz.ByubVV1zmae3i.M3kiIelaqbBW', 'userIsakki.jpeg', 'yuva3210@gmail.com', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `popular_item`
--
ALTER TABLE `popular_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `popular_item`
--
ALTER TABLE `popular_item`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
