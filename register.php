<?php ob_start();
include './includes/db.php';
include './includes/head.php';
?>
<style>
    .bg-register-image {
        background: url("./img/brooke-lark-W1B2LpQOBxA-unsplash.jpg");
        background-position: center;
        background-size: cover;
    }
</style>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-register-image"></div>

                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-white-900"><i class="bi bi-bag-check"></i> ShopNew</h1>
                                    <h1 class="h4 text-white-900 mb-3">Register Now!</h1>
                                </div>
                                <form action="register.php" method="post">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                        <input type="text" class="form-control" placeholder="Username" aria-label="Username" name="username" aria-describedby="basic-addon1">
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">First Name</span>
                                        <input type="text" aria-label="First name" class="form-control" name="f_name" placeholder="First Name">
                                        <!-- <input type="text" aria-label="Last name" class="form-control" name="l_name" placeholder="Last Name"> -->
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Last Name</span>
                                        <!-- <input type="text" aria-label="First name" class="form-control" name="f_name" placeholder="First Name"> -->
                                        <input type="text" aria-label="Last name" class="form-control" name="l_name" placeholder="Last Name">
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Email" name="email" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                        <span class="input-group-text" id="basic-addon2">@example.com</span>
                                    </div>
                                    <div class="input-group mb-3">
                                        <span class="input-group-text" id="basic-addon1">Password</span>
                                        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-3 text-center" name="register">REGISTER</button>
                                    <p class="text-center">
                                        If you already have an Account <a href="login.php">Login</a>
                                    </p>
                                </form>
                                <hr>
                                <?php
                                if (isset($_POST['register'])) {
                                    $username = mysqli_real_escape_string($connection, $_POST['username']);
                                    $email = mysqli_real_escape_string($connection, $_POST['email']);
                                    $password = mysqli_real_escape_string($connection, $_POST['password']);
                                    $password = crypt($password, '$2a$07$usesomesillystringisakkik$');
                                    $f_name = $_POST['f_name'];
                                    $l_name = $_POST['l_name'];

                                    $select_user_query = "SELECT * FROM users";
                                    $select_user_result = mysqli_query($connection, $select_user_query);

                                    while ($row = mysqli_fetch_assoc($select_user_result)) {
                                        $db_username = $row['username'];
                                        $db_email = $row['user_email'];
                                        $db_first_name = $row['user_firstname'];
                                        $db_last_name = $row['user_lastname'];
                                        $db_password = $row['user_password'];
                                        $db_image = $row['user_image'];
                                    }

                                    if (empty($username) || empty($password) || empty($email) || empty($f_name) || empty($l_name)) {
                                        echo '<div class="alert alert-danger" role="alert">
                                    Please fill all the Fields..!!
                                    </div>';
                                    } elseif ($username == $db_username || $email == $db_email) {
                                        echo '<div class="alert alert-danger" role="alert">
                                        Email or Username is already exists Click here to <a href="login.php">Login</a>
                                    </div>';
                                    } else {
                                        $insert_user = "INSERT INTO users (username , user_firstname , user_lastname , user_password , user_image , user_email , user_role) VALUES ('$username' , '$f_name' , '$l_name' , '$password' , 'avatar.png' , '$email' , 'user')";
                                        $insert_query  = mysqli_query($connection, $insert_user);
                                        echo '<div class="alert alert-success" role="alert" text-white>
                                    Successfully Registered Click here to <a href="login.php">Login</a>
                                    </div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 'user') {
        header('location: ./index.php');
    }
}
?>