<?php ob_start();
include './includes/db.php';
include './includes/head.php';

?>
<style>
    .bg-login-image {
        background: url("./img/nathan-dumlao-lvWw_G8tKsk-unsplash.jpg");
        background-position: center;
        background-size: cover;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-white-900 mb-4">Welcome back! <br><i class="bi bi-bag-check"></i> ShopNew</h1>
                                </div>
                                <form action="login.php" method="post">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                                        <input type="email" placeholder="Enter Email..." class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" autocomplete="off">
                                        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Enter Password...">
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-3" name="login">LOGIN</button>
                                    <!-- <p class="text-center">New User <a href="register.php">Register Now</a></p> -->
                                    <hr>

                                    <div class="text-center">
                                        <a class="small" href="forget-password.php">Forget Password..</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="register.php">Create an Account!</a>
                                    </div>
                                </form>
                                <?php
                                if (isset($_POST['login'])) {
                                    $email = $_POST['email'];
                                    $password = $_POST['password'];

                                    $select_user_query = "SELECT * FROM users WHERE user_email = '$email'";
                                    $select_user_result = mysqli_query($connection, $select_user_query);

                                    while ($row = mysqli_fetch_assoc($select_user_result)) {
                                        $db_username = $row['username'];
                                        $db_password = $row['user_password'];
                                        $db_role = $row['user_role'];
                                        $db_email = $row['user_email'];
                                        $db_image = $row['user_image'];
                                        $db_id = $row['user_id'];
                                    }

                                    if (crypt($password, $db_password) === $db_password) {
                                        $password = true;
                                    }
                                    if ($email !== $db_email || $password !== true) {
                                        echo '<div class="alert alert-danger" role="alert">
                                               Incorrect Username Or Password
                                            </div>';
                                    } else if ($email == $db_email && $password == true && $db_role == 'user') {
                                        header('location: ./index.php');
                                        $_SESSION['username'] = $db_username;
                                        $_SESSION['id'] = $db_id;
                                        $_SESSION['role'] = $db_role;
                                        $_SESSION['img'] = $db_image;
                                    } else if ($email == $db_email && $password == true && $db_role == 'Admin') {
                                        header('location: ./admin/index.php');
                                        $_SESSION['username'] = $db_username;
                                        $_SESSION['role'] = $db_role;
                                        $_SESSION['email'] = $db_email;
                                        $_SESSION['img'] = $db_image;
                                        $_SESSION['id'] = $db_id;
                                    } else {
                                        echo 'Not Matched to the DB';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 'user') {
        header('location: ./index.php');
    }
}
?>