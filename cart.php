<?php ob_start();
include './includes/head.php';
include './includes/nav.php';
include './includes/db.php';

$pop_id = $_GET['pop_id'];

$select_item = "SELECT * FROM popular_item WHERE id = $pop_id";
$select_item_query = mysqli_query($connection, $select_item);

while ($row = mysqli_fetch_assoc($select_item_query)) {
    $pop_name = $row['pop_name'];
    $pop_price = $row['pop_price'];
    $pop_type = $row['dress_type'];
    $pop_star = $row['pop_star'];
    $pop_img = $row['pop_img'];
}

?>


<section class="py-5">
    <div class="container px-4 px-lg-5 my-5">
        <div class="row gx-4 gx-lg-5 align-items-center">
            <div class="col-md-6 w-50"><img src="./img/<?php echo $pop_img ?>" alt="..." /></div>
            <div class="col-md-6">
                <div class="small mb-1">SKU: BST-498</div>
                <h1 class="display-5 fw-bolder"><?php echo $pop_name ?></h1>
                <div class="fs-5 mb-5">
                    <span>$<?php echo $pop_price ?></span>
                </div>
                <div class="fs-5 mb-5">
                    <div class="d-flex justify-content small text-warning mb-2">
                        <?php
                        for ($i = 0; $i < $pop_star; $i++) {
                        ?>
                            <div class="bi-star-fill"></div>
                            <?php }
                        if ($pop_star < 5) {
                            for ($bal = $pop_star; $bal < 5; $bal++) {
                            ?>
                                <div class="bi-star"></div>
                        <?php }
                        }
                        ?>
                    </div>
                </div>
                <p class="lead">Lorem ipsum dolor sit amet consectetur adipisicing elit. Praesentium at dolorem quidem modi. Nam sequi consequatur obcaecati excepturi alias magni, accusamus eius blanditiis delectus ipsam minima ea iste laborum vero?</p>


                <?php
                $db_dress_id = NULL;
                $user_id = $_SESSION['id'];
                if (isset($_POST['add_cart'])) {
                    $quantity = $_POST['quantity'];
                    $size = $_POST['size'];

                    $cart_select_all = "SELECT * FROM cart WHERE user_id = '$user_id'";
                    $cart_select_all_res = mysqli_query($connection, $cart_select_all);
                    while ($row = mysqli_fetch_assoc($cart_select_all_res)) {
                        $db_user_id = $row['user_id'];
                        $db_dress_id = $row['pop_id'];
                        $db_quantity = $row['quantity'];
                    }

                    if ($pop_id == $db_dress_id && $user_id == $db_user_id) {
                        $db_quantity += $quantity;
                        $cart_update = "UPDATE cart SET quantity = '$db_quantity' WHERE pop_id = '$pop_id'";
                        $cart_res = mysqli_query($connection, $cart_update);
                        // header('location:./cart.php');
                        header("location:javascript://history.go(-1)");
                        header('location:shop_cart_.php');
                    } else {
                        $cart_query = "INSERT INTO cart (user_id , pop_id , quantity , size) VALUES ('$user_id' , '$pop_id' , $quantity, '$size' )";
                        $cart_res = mysqli_query($connection, $cart_query);
                        // header('location:./cart.php');
                        header("location:javascript://history.go(-1)");
                        header('location:shop_cart_.php');
                    }
                }
                ?>

                <form action="" method="post">
                    <div class="d-flex">
                        <!-- <?php
                                echo $_SESSION['id'];
                                echo $pop_id;
                                ?> -->
                        <input class="form-control text-center me-3" id="inputQuantity" type="num" value="1" style="max-width: 3rem" name="quantity" />
                        <select class="form-select" aria-label="Default select example" name="size">
                            <option selected>Size</option>
                            <option value="M">M</option>
                            <option value="S">S</option>
                            <option value="XS">XS</option>
                            <option value="L">L</option>
                            <option value="XL">XL</option>
                            <option value="XXL">XXL</option>
                        </select>
                        <button class="btn btn-outline-dark flex-shrink-0 m-1" type="submit" name="add_cart">
                            <i class="bi-cart-fill me-1"></i>
                            Add to cart
                        </button>
                        <button class="btn btn-outline-dark flex-shrink-0 m-1" type="submit">
                            <i class="bi-cart-fill me-1"></i>
                            Buy Now
                        </button>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php
include './related_items.php';
include './includes/footer.php';
?>