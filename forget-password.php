<?php ob_start();
include './includes/db.php';
include './includes/head.php';

?>
<style>
    .bg-password-image {
        background: url("./img/tech-daily-pz_L0YpSVvE-unsplash.jpg");
        background-position: center;
        background-size: cover;
    }
</style>

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                                    <p class="mb-4">We get it, stuff happens. Just enter your email address below
                                        and we'll Check the You'll Update the Password!</p>
                                </div>

                                <?php
                                $user_email = NULL;
                                if (isset($_POST['reset'])) {
                                    $user_email = $_POST['email'];
                                    $_SESSION['user_email'] = $user_email;
                                    $email_query = "SELECT * FROM users WHERE user_email = '$user_email'";
                                    $email_res = mysqli_query($connection, $email_query);
                                    $db_email = NULL;
                                    $db_password = NULL;
                                    while ($row = mysqli_fetch_assoc($email_res)) {
                                        $db_email = $row['user_email'];
                                        $db_password = $row['user_password'];
                                    }

                                    if (!empty($user_email) && $user_email == $db_email) {
                                        echo
                                        '<form method="post">
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Confirm Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" name="C_password" placeholder="Confirm Password">
                                    </div>

                                    <button class="btn btn-primary btn-user btn-block mt-3" name="reset_pass" type="submit">
                                        Reset Password
                                    </button>
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="register.php">Create an Account!</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="login.php">Already have an account? Login!</a>
                                </div>';
                                    } else {
                                        echo '<div class="alert alert-danger" role="alert">
                                               This Type of Email is Not There...<a class="small" href="register.php">Create an Account!</a>
                                            </div>';
                                    }
                                } else {

                                    echo '<form method="post" action="forget-password.php">
                                    <div class="form-group">
                                        <input type="email" name ="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                    </div>

                                    <button class="btn btn-primary btn-user btn-block mt-3" name="reset" type="submit">
                                        Continue
                                    </button>
                                </form>
                                
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="register.php">Create an Account!</a>
                                </div>
                                <div class="text-center">
                                    <a class="small" href="login.php">Already have an account? Login!</a>
                                </div>';
                                }
                                if (isset($_POST['reset_pass'])) {
                                    $password = $_POST['password'];
                                    $confirm_password = $_POST['C_password'];
                                    $user_email = $_SESSION['user_email'];
                                    if (!empty($password) && !empty($confirm_password) && $password == $confirm_password) {
                                        $password = mysqli_real_escape_string($connection, $password);
                                        $password = crypt($password, '$2a$07$usesomesillystringisakkik$');
                                        $confirm_password = mysqli_real_escape_string($connection, $confirm_password);
                                        $confirm_password = crypt($confirm_password, '$2a$07$usesomesillystringisakkik$');

                                        $update_pass_query = "UPDATE users SET user_password = '$password' WHERE user_email = '$user_email'";
                                        $update_pass_res = mysqli_query($connection, $update_pass_query);
                                        session_destroy();
                                        header('location:login.php');
                                    } else {
                                        echo '<div class="alert alert-danger mt-4" role="alert">
                                               Try Again Password is Not Matched...
                                            </div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($_SESSION['role'])) {
    if ($_SESSION['role'] == 'user') {
        header('location: ./index.php');
    }
}
?>