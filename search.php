<?php
include './includes/head.php';
include './includes/nav.php';
include './includes/db.php';
$search = $_POST['search']; 
?>



<!-- Header-->
<?php
include './includes/header.php';
?>

<!-- Section-->
<section class="py-5">
    <div class="container px-4 px-lg-5 mt-5">
        <h2 style="padding-bottom: 20px;">
            SEARCHED ITEMS
        </h2>
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">

            <?php
            $search_item = "SELECT * FROM popular_item WHERE pop_name LIKE '%$search%'";
            $search_item_query = mysqli_query($connection, $search_item);

            while ($row = mysqli_fetch_assoc($search_item_query)) {
                $pop_name  =   $row['pop_name'];
                $pop_img  =   $row['pop_img'];
                $pop_price  =   $row['pop_price'];
                $pop_id = $row['id'];
                $pop_star  =   $row['pop_star'];
            ?>

                <div class="col mb-5">
                    <div class="card h-100">
                        <!-- Product image-->

                        <img src="./img/<?php echo $pop_img ?>" class="card-img-top" alt="..." style="height:300px">
                        <!-- Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name-->
                                <h5 class="fw-bolder p-1"><?php echo $pop_name ?></h5>
                                <!-- Product reviews-->

                                <div class="d-flex justify-content-center small text-warning mb-2">
                                    <?php
                                    for ($i = 0; $i < $pop_star; $i++) {
                                    ?>
                                        <div class="bi-star-fill"></div>
                                        <?php }
                                    if ($pop_star < 5) {
                                        for ($bal = $pop_star; $bal < 5; $bal++) {
                                        ?>
                                            <div class="bi-star"></div>
                                    <?php }
                                    }
                                    ?>
                                </div>
                                <div>
                                    $<?php echo $pop_price ?>
                                </div>
                            </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="./cart.php?pop_id=<?php echo $pop_id; ?>">Add to cart</a></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- <div class="col mb-5">
                <div class="card h-100">
                    <img class="card-img-top" src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." />
                    <div class="card-body p-4">
                        <div class="text-center">
                            <h5 class="fw-bolder">Fancy Product</h5>
                            $40.00 - $80.00
                        </div>
                    </div>
                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">View options</a></div>
                    </div>
                </div>
            </div> -->


        </div>
    </div>
</section>
<!-- Footer-->
<?php
include './includes/footer.php'
?>