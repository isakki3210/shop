<?php
$related_query = "SELECT * FROM popular_item WHERE dress_type = '$pop_type' AND id != $pop_id";
$related_result = mysqli_query($connection, $related_query);

$search_count = mysqli_num_rows($related_result);

?>


<section class="py-5 bg-light">
    <div class="container px-4 px-lg-5 mt-5">
        <h2 class="fw-bolder mb-4">Related products</h2>
        <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            <?php

            if ($search_count === 0) {
                echo '<div class="alert alert-danger" role="alert">
                            No Related Items
                        </div>';
            }
            while ($row = mysqli_fetch_assoc($related_result)) {

                $related_name = $row['pop_name'];
                $related_price = $row['pop_price'];
                $related_id = $row['id'];
                $related_star = $row['pop_star'];
                $related_img = $row['pop_img'];
            ?>
                <div class="col mb-5">
                    <div class="card h-100">
                        <!-- Sale badge-->
                        <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Sale</div>
                        <!-- Product image-->
                        <img class="card-img-top" src="./img/<?php echo $related_img ?>" alt="..." style="height:300px" />
                        <!-- Product details-->
                        <div class="card-body p-4">
                            <div class="text-center">
                                <!-- Product name-->
                                <h5 class="fw-bolder"><?php echo $related_name ?></h5>
                                <!-- Product reviews-->
                                <div class="d-flex justify-content-center small text-warning mb-2">
                                    <?php
                                    for ($i = 0; $i < $pop_star; $i++) {
                                    ?>
                                        <div class="bi-star-fill"></div>
                                        <?php }
                                    if ($pop_star < 5) {
                                        for ($bal = $pop_star; $bal < 5; $bal++) {
                                        ?>
                                            <div class="bi-star"></div>
                                    <?php }
                                    }
                                    ?>
                                </div>
                                <!-- Product price-->
                                <!-- <span class="text-muted text-decoration-line-through">$20.00</span> -->
                                $<?php echo $related_price ?>
                            </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent d-flex justify-content-center">
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto m-1" href="./cart.php?pop_id=<?php echo $related_id; ?>">Add to cart</a></div>
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="./cart.php?pop_id=<?php echo $related_id; ?>">Buy Now</a></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>